package com.entando.samples.springbootkc.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Progetto {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @NonNull
    private String nome;
    @NonNull
    private String descrizione;
    @NonNull
    private LocalDate dataInizio;

    @ManyToOne
    @JsonIgnoreProperties(value = "progetti", allowSetters = true)
    private Comune comune;


}
