package com.entando.samples.springbootkc.repository;

import com.entando.samples.springbootkc.entity.Comune;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ComuneRepository extends CrudRepository<Comune, Long> {

//    Comune findByNome(String nome);
//
////    Iterable<Comune> findAll();
//
//    Comune findById(long id);
}