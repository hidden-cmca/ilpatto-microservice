package com.entando.samples.springbootkc.repository;

import com.entando.samples.springbootkc.entity.Progetto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProgettoRepository extends CrudRepository<Progetto, Long> {

//    List<Progetto> findByName(String name);
//
//    Progetto findById(long id);
}