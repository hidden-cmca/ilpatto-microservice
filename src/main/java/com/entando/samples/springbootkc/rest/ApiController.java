package com.entando.samples.springbootkc.rest;

import com.entando.samples.springbootkc.entity.Comune;
import com.entando.samples.springbootkc.entity.Progetto;
import com.entando.samples.springbootkc.repository.ComuneRepository;
import com.entando.samples.springbootkc.repository.ProgettoRepository;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.*;

@RestController
@RequestMapping("/api")
@Slf4j
public class ApiController {

    //private List<Progetto> listaProgetti = new ArrayList<>();
    //List<Comune> listaComuni = new ArrayList<>();

    private Map<String, List<Progetto>> store = new HashMap<>();
    private ComuneRepository crepository;
    private ProgettoRepository prepository;

    public ApiController(ComuneRepository crepository, ProgettoRepository prepository){
      this.crepository = crepository;
      this.crepository.save(new Comune("Maracalagonis"));
      this.crepository.save(new Comune("Cagliari"));

      this.prepository = prepository;
    }

    @RolesAllowed({"ilpatto-admin", "ilpatto-user"})
    @CrossOrigin
    @GetMapping("/comuni")
    public Iterable<Comune> getListaComuniCittaMetropolitana() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return this.crepository.findAll();
    }

    @RolesAllowed({"ilpatto-admin", "ilpatto-user"})
    @CrossOrigin
    @GetMapping("/progetto/{id}")
    public Optional<Progetto> getListaProgettiPerIdCittaMetropolitana(@PathVariable Long id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return this.prepository.findById(id);
    }

    @RolesAllowed({"ilpatto-admin", "ilpatto-user"})
    @CrossOrigin
    @GetMapping("/infoprogetti")
    public Iterable<Progetto> getListaProgettiCittaMetropolitana() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return this.prepository.findAll();
    }

    @RolesAllowed({"ilpatto-admin", "ilpatto-user"})
    @CrossOrigin
    @GetMapping("/infoprogetti")
    public List<InfoProgetto> getListaInfoProgettiCittaMetropolitana() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<InfoProgetto> listaProgetti = new ArrayList<>();

        Iterator<Progetto> iter = this.prepository.findAll().iterator();
        while (iter.hasNext()) {
            InfoProgetto prj = new InfoProgetto(iter.next().getNome(), iter.next().getDescrizione());
            listaProgetti.add( prj );
        }
//        this.listaProgetti.add(new Progetto("1", "progetto1", "prova progetto"));
//        this.listaProgetti.add(new Progetto("2", "progetto2", "prova progetto"));
        return listaProgetti;
    }

    @RolesAllowed({"ilpatto-admin"})
    @CrossOrigin
    @PostMapping("/progetti")
    public void putProgettoCittaMetropolitana(@RequestBody Progetto progetto) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        this.prepository.save(progetto);
    }


//    @Data
//    public static class Comune {
//        @NonNull
//        private final String id;
//        @NonNull
//        private final String nome;
//    }



    @Data
    @RequiredArgsConstructor
    public static class InfoProgetto {

        @NonNull
        private String nome;
        @NonNull
        private String descrizione;


    }




  //idComune, nomeComune
  //{idProgetto, nomeProgetto, descrizioneProgetto}
}
