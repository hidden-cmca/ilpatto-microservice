package com.entando.samples.springbootkc.service;

//import com.entando.samples.springbootkc.entity.Comune;
//import com.entando.samples.springbootkc.repository.ComuneRepository;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.Bean;
//
//@SpringBootApplication
//public class AccessingDataJpaComune {
//
//    private static final Logger log = LoggerFactory.getLogger(AccessingDataJpaComune.class);
//
//    public static void main(String[] args) {
//        SpringApplication.run(AccessingDataJpaComune.class);
//    }
//
//    @Bean
//    public CommandLineRunner demo(ComuneRepository repository) {
//        return (args) -> {
//            // save a few customers
//            repository.save(new Comune("Maracalagonis"));
//            repository.save(new Comune("Cagliari"));
//
//
//            // fetch all customers
//            log.info("Customers found with findAll():");
//            log.info("-------------------------------");
//            for (Comune comune : repository.findAll()) {
//                log.info(comune.toString());
//            }
//            log.info("");
//
//            // fetch an individual customer by ID
//            Comune comune = repository.findById(1L);
//            log.info("Customer found with findById(1L):");
//            log.info("--------------------------------");
//            log.info(comune.toString());
//            log.info("");
//
//            // fetch customers by last name
//            log.info("Customer found with findByLastName('Bauer'):");
//            log.info("--------------------------------------------");
//            repository.findByLastName("Bauer").forEach(bauer -> {
//                log.info(bauer.toString());
//            });
//            // for (Customer bauer : repository.findByLastName("Bauer")) {
//            // 	log.info(bauer.toString());
//            // }
//            log.info("");
//        };
//    }
//
//}
